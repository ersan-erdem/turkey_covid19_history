import pandas as pd 
import requests 
from bs4 import BeautifulSoup
import xlwt
import os
import re
import xlsxwriter
import numpy as np
import math
url="https://covid19.saglik.gov.tr/TR-66935/genel-koronavirus-tablosu.html"

response=requests.get(url)

soup=BeautifulSoup(response.text,"html.parser")

row=soup.find_all("script")

Data=[]
rdate=row[len(row)-1].string.split("},{")
rdate1=row[len(row)-1].string.split("},{")[1].split('","')
r1data=dict()
r1=row[len(row)-1].string.split("},{")[0].split("\r\n//<![CDATA[\r\nvar geneldurumjson = [{")
del r1[0]
for i in range(len(rdate1)):
    r1data.update({re.sub(r'["]+', '', r1[0].split('","')[i].split(":")[0]).strip():re.sub(r'["]+', '', r1[0].split('","')[i].split(":")[1]).strip()})
Data.append(r1data)

r2data=[]
for i in range(1,len(rdate)-1):
    s=dict({})
    for j in range(len(rdate1)):
        s.update({re.sub(r'["]+', '', rdate[i].split('","')[j]).strip().split(":")[0]:re.sub(r'["]+', '', rdate[i].split('","')[j]).strip().split(":")[1]})
    r2data.append(s)

   
for i in range(len(r2data)):
    Data.append(r2data[i])

r3=row[len(row)-1].string.split("},{")[len(rdate)-1].split("}];//]]>\r\n")[0].split('","')
r3data=dict({})
for i in range(len(rdate1)):
    r3data.update({re.sub(r'["]+', '', r3[i].split(':')[0]).strip():re.sub(r'["]+', '', r3[i].split(':')[1]).strip()})
Data.append(r3data)

Data.reverse()

filepath = os.path.normpath(os.path.join('C:/Users/ersan/Desktop', 'veriler.txt'))
myfile = open(filepath, 'w+')
mytxt = myfile.read()
myfile.write("Date_Number   Total_Cases      Total_Deaths   Total_Recovered  Daily_Cases Daily_Deaths Daily_Recovered  Daily_Patient  \n")
for i in range(len(Data)):
    myfile.write("{0} {1}    {2}    {3}   {4}    {5}  {6} {7} \n".format(Data[i]['tarih'], Data[i]['toplam_hasta'], Data[i]['toplam_vefat'], Data[i]['toplam_iyilesen'],
    Data[i][ 'gunluk_vaka'], Data[i]['gunluk_vefat'], Data[i]['gunluk_iyilesen'], Data[i]['gunluk_hasta'] ))
myfile.close()

book = xlwt.Workbook()
ws = book.add_sheet('Veriler') # Add a sheet

filepath1=os.path.normpath(os.path.join('C:/Users/ersan/Desktop', 'veriler.txt'))

excl = open(filepath1, 'r')
data = excl.readlines() # read all lines at once
for i in range(len(data)):
    row = data[i].split()  # This will return a line of string data, you may need to convert to other formats depending on your use cas
    for j in range(len(row)):
        ws.write(i, j, row[j])  # Write to cell i, j
book.save(os.path.normpath(os.path.join('C:/Users/ersan/Desktop', "Covid-19-veriler.xls")))
excl.close()

